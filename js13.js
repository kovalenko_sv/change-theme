'use strick'

window.onload = function () {
  const currentTheme = localStorage.getItem("theme");
  if (currentTheme) {
    document.body.classList.add(currentTheme);
  }
};

const btnChange = document.createElement("button");
btnChange.textContent = "Click me to change theme!";
btnChange.style.cssText = "padding: 20px; font-size:15px; background-color:red";
document.body.prepend(btnChange);

btnChange.addEventListener("click", () => {
  if (document.body.classList.contains("light_theme")) {
    document.body.classList.remove("light_theme");
    document.body.classList.add("dark_theme");
    localStorage.setItem("theme", "dark_theme");
  } else {
    document.body.classList.remove("dark_theme");
    document.body.classList.add("light_theme");
    localStorage.setItem("theme", "light_theme");
  }
});
